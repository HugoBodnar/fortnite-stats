# **Fortnite Stats - Documentation**
## **Team project**
<ul>
<li>Matthieu Serraz <a href="mailto:Matthieu.Serraz@etu.univ-grenoble-alpes.fr">Contact</a></li>
<li>Romain Bourdon <a href="mailto:Romain.Bourdon@etu.univ-grenoble-alpes.fr">Contact</a></li>
<li>Hugo Bodenart <a href="mailto:Hugo.Bodnar@etu.univ-grenoble-alpes.fr">Contact</a></li>
</ul>

## Specifications

### Aim of project

**Get statisctics about Fortnite players**

### Organisation
**Homepage :** You can choose your page destination
**Statistics :** You will fill your platform and pseudo to get your stats of your games (KDA, Top1, Matchs, Kills, Time played)
**Challenges :** Get your challenges Fortnite
**News :** Get the actuality of Fortnite
### Use Cases diagram
<a href="https://www.noelshack.com/2019-23-4-1559816304-usecase.png"><img src="https://image.noelshack.com/minis/2019/23/4/1559816304-usecase.png" border="0" alt="1559816304-usecase.png - envoi d'image avec NoelShack" title="1559816304-usecase.png"/></a>

### Check Player Stats
<a href="https://www.noelshack.com/2019-23-4-1559816304-checkstats.png"><img src="https://image.noelshack.com/minis/2019/23/4/1559816304-checkstats.png" border="0" alt="1559816304-checkstats.png - envoi d'image avec NoelShack" title="1559816304-checkstats.png"/></a>

### Check News
<a href="https://www.noelshack.com/2019-23-4-1559816304-checknews.png"><img src="https://image.noelshack.com/minis/2019/23/4/1559816304-checknews.png" border="0" alt="1559816304-checknews.png - envoi d'image avec NoelShack" title="1559816304-checknews.png"/></a>

### Check Game Challenge
<a href="https://www.noelshack.com/2019-23-4-1559816304-checkgamechallenge.png"><img src="https://image.noelshack.com/minis/2019/23/4/1559816304-checkgamechallenge.png" border="0" alt="1559816304-checkgamechallenge.png - envoi d'image avec NoelShack" title="1559816304-checkgamechallenge.png"/></a>

**Application presentation**

Our application is based on the video game "Fortnite". It allows us to have access to all the statistics informations about a player which is identified by his ID and his pseudo. Moreover it give access to all the fortnite news and the weekly challenge list.
This application is made with one homepage and three other pages. One page for one functionality.
