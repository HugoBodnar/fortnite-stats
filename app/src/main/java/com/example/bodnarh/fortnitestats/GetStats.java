package com.example.bodnarh.fortnitestats;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class GetStats extends AsyncTask<String, Void, String> {
    TextView textView;
    public GetStats(TextView textView) {
        this.textView = textView;
    }

    protected String doInBackground(String... strings) {
        try {
            URL url = new URL("https://fortnite-api.theapinetwork.com/prod09/users/public/br_stats_v2?user_id=" + strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("Authorization", "3a3ab16ab08705ba04bb0991067deaa0");

            urlConnection.setRequestMethod("GET");


            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            return in.readLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        catch (Exception ex)
        {
            Log.wtf("Oops", ex.getMessage());
        }
        return null;
    }

    protected void onPostExecute(String s) {
        JSONObject jsonObj = null;
        try {
            Log.v("Stats", s);
            jsonObj = new JSONObject(s);
            JSONArray keyboardmouse = jsonObj.getJSONObject("data").getJSONObject("stats").getJSONArray("keyboardmouse");
            JSONObject keyboardmouseObj = keyboardmouse.getJSONObject(0);
            JSONArray entries = keyboardmouseObj.getJSONArray("entries");
            JSONObject entriesObj = entries.getJSONObject(0);
            String matchesplayed = Integer.toString(entriesObj.getJSONObject("stats").getInt("matchesplayed"));
            String minutesplayed = Integer.toString(entriesObj.getJSONObject("stats").getInt("minutesplayed"));
            String score = Integer.toString(entriesObj.getJSONObject("stats").getInt("score"));
            String result = "Matchs : " + matchesplayed + ", Minutes jouées : " + minutesplayed + ", Score : " + score;
            this.textView.setText(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
