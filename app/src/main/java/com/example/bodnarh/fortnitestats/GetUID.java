package com.example.bodnarh.fortnitestats;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class GetUID extends AsyncTask<String, Void, String> {
    TextView textView;
    public GetUID(TextView textView) {
        this.textView = textView;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL("https://fortnite-public-api.theapinetwork.com/prod09/users/id?username=" + strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");


            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            return in.readLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        catch (Exception ex)
        {
            Log.wtf("Oops", ex.getMessage());
        }
        return null;
    }

    protected void onPostExecute(String s) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(s);
            String uid = jsonObj.getString("uid");
            Log.v("UID : ", uid) ;
            GetStats getStats = new GetStats(this.textView);
            getStats.execute(uid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
