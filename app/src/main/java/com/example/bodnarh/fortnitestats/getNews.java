package com.example.bodnarh.fortnitestats;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class getNews extends AsyncTask<String, Void, String> {
    TextView text;
    public getNews(TextView text) {
        this.text = text;
    }

    protected String doInBackground(String... strings) {
        try {
            URL url = new URL("https://fortnite-api.theapinetwork.com/br_motd/get\n");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty ("Authorization", "3a3ab16ab08705ba04bb0991067deaa0");

            urlConnection.setRequestMethod("GET");


            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            return in.readLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        catch (Exception ex)
        {
            Log.wtf("Oops", ex.getMessage());
        }
        return null;
    }

    protected void onPostExecute(String s) {
        JSONObject jsonObj = null;
        try {
            Log.v("Stats", s);
            jsonObj = new JSONObject(s);
            JSONArray arr = jsonObj.getJSONArray("data");
            String title = "";
            for(int i = 0; i < arr.length(); i++) {
                title += arr.getJSONObject(i).getString("title") + ", ";

            }
            this.text.setText(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
