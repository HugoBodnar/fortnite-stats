package com.example.bodnarh.fortnitestats;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Stats extends AppCompatActivity {
    URL url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
    }

    public void getUserUid(View view) throws IOException {
        EditText pseudoTv = findViewById(R.id.editText);
        String pseudo = (String) pseudoTv.getText().toString();
        TextView textView = (TextView) findViewById(R.id.jsonResult);
        GetUID getStats = new GetUID(textView);
        getStats.execute(pseudo);


    }
}
