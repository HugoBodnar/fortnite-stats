package com.example.bodnarh.fortnitestats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void lunchStatsActivity(View view) {
        Intent intent = new Intent(this, Stats.class);
        startActivity(intent);

    }

    public void openNewsActivity(View view) {
        Intent intent = new Intent(this, News.class);
        startActivity(intent);
    }
}
